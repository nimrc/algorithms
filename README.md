### 常见算法和数据结构 (JavaScript实现)

[![Build Status](https://travis-ci.org/fyibmsd/algorithms.svg?branch=master)](https://travis-ci.org/fyibmsd/algorithms)


#### 链式结构
- 单链表 (linked list)
- 双向链表 (double linked list)
- 队列 (queue)
- 栈 (stack)

### 树形结构
- 二叉树
    - 完全二叉树
    - 遍历
    - 反转
- 二叉查找树 (binary search tree)

#### 排序算法
- 冒泡排序 (bubble sort)
- 快速排序 (quick sort)
- 插入排序 (insertion sort)
