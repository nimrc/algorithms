'use strict';

/**
 * Bubble sort
 * T = O(n^2)
 *
 * @param array {Array}
 *
 * @return Array
 * */
export default function BubbleSort(array) {
    for (let i = 0; i < array.length; i++) {
        for (let j = 0; j < array.length - i - 1; j++) {
            if (array[j] > array[j + 1]) {
                let tmp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = tmp;
            }
        }
    }

    return array;
}
